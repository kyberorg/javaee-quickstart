package net.virtalab.prod.model;

import javax.ejb.Stateless;

/**
 * Stub Bean
 * <p/>
 * User: asm
 * Date: 7/16/13
 * Time: 8:44 AM
 */
@Stateless
public class UserStubBean implements StubBean {

    @Override
    public String info() {
        return String.format("I am %s",UserStubBean.class.getSimpleName());

    }


}
