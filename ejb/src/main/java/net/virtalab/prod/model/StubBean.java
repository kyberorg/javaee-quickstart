package net.virtalab.prod.model;

import javax.ejb.Local;

/**
 * Stub beans interface
 * <p/>
 * User: asm
 * Date: 7/16/13
 * Time: 8:45 AM
 */
@Local
public interface StubBean {
    String info();
}
