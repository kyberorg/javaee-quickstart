package net.virtalab.prod;

import net.virtalab.prod.model.StubBean;

import javax.ejb.EJB;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;

/**
 * Just Stub Servlet that provides information about connection
 * <p/>
 * User: asm
 * Date: 7/16/13
 * Time: 8:08 AM
 */
public class InfoServlet extends HttpServlet {
    private  static final String CONTENT_TYPE = "text/html; charset=UTF-8";
    private PrintWriter out = null;
    private String lt = "<BR>";
    @EJB
    private StubBean bean;

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        out = response.getWriter();
        response.setContentType(CONTENT_TYPE);

        String ip = request.getRemoteAddr();
        int port = request.getRemotePort();
        String method = request.getMethod();
        String conn = request.getHeader("Connection");
        String ua = request.getHeader("User-Agent");
        String lang = request.getHeader("Accept-Language");

        String info = String.format(
                new StringBuilder()
                        .append("<h3>Your connection</h3>")
                        .append("<table>")
                            .append("<tr>")
                                .append("<td>IP Address</td>")
                                .append("<td>%s</td>")
                            .append("</tr>")
                            .append("<tr>")
                                .append("<td>Remote port</td>")
                                .append("<td>%s</td>")
                            .append("</tr>")
                            .append("<tr>")
                                .append("<td>Method</td>")
                                .append("<td>%s</td>")
                            .append("</tr>")
                            .append("<tr>")
                                .append("<td>Connection</td>")
                                .append("<td>%s</td>")
                            .append("</tr>")
                            .append("<tr>")
                                .append("<td>User Agent</td>")
                                .append("<td>%s</td>")
                            .append("</tr>")
                            .append("<tr>")
                                .append("<td>Language</td>")
                                .append("<td>%s</td>")
                            .append("</tr>")
                        .append("</table>").toString(),

                ip,port,method,conn,ua,lang
        );

        String beanInfo = String.format(new StringBuilder("<h3>EJB Info</h3>")
                .append("Bean info: %s")
                .toString(),
                bean.info());

        out.print(info+lt);
        out.print(beanInfo+lt);

    }
}
